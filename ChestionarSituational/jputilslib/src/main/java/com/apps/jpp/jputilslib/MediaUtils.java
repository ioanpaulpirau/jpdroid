package com.apps.jpp.jputilslib;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by JP on 18-Jan-16.
 * Contains utility functions for in-app use
 */
public class MediaUtils {
    private Context applicationContext;
    private MediaPlayer mediaPlayer = new MediaPlayer();

    /****
     * default constructor
     * @param applicationContext context of the app using this class
     */
    public MediaUtils(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    /***
     * play sounds from files stored in assets folder
     *
     * @param fileName  name of the file including extension
     * @param overwrite command wether to interrupt file that is being currently played
     */
    public void playSoundSync(String fileName, boolean overwrite) {
        try {
            AssetFileDescriptor afd;
            afd = applicationContext.getAssets().openFd(fileName);

            if (!mediaPlayer.isPlaying() || overwrite) {
                mediaPlayer.reset();
                mediaPlayer.setDataSource(afd.getFileDescriptor(),
                        afd.getStartOffset(),
                        afd.getLength());
                mediaPlayer.prepareAsync();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.apps.jpp.jputilslib;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by JP on 19-Jan-16.
 * Contains utility functions for in-app use
 */
public class FileUtils {
    private Context applicationContext;

    /****
     * default constructor
     *
     * @param applicationContext context of the app using this class
     */
    public FileUtils(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    /****
     * loads a text file from the assets and returns it as a string
     *
     * @param fileName name of the file stored in the assets
     * @returns string with file contents
     */
    public String loadTextFileFromAsset(String fileName) {
        String fileContents;
        try {
            InputStream is = applicationContext.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            fileContents = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return fileContents;
    }

    /****
     * loads a text file from the internal storage (app private folder) and returns it as a string
     *
     * @param fileName name of the file we want to read
     * @return string with file contents
     */
    public String loadTextFileFromInternalStorage(String fileName) {
        String fileContents = "";
        FileInputStream fis = null;

        try {
            fis = applicationContext.openFileInput(fileName);
            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            fileContents = new String(buffer, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileContents;
    }

    /****
     * saves a text file to internal storage
     *
     * @param filename     name of the file
     * @param fileContents contents of the file
     * @return true if successful, false otherwise
     */
    public boolean saveTextFileToInternalStorage(String filename, String fileContents) {
        FileOutputStream fos = null;
        try {
            fos = applicationContext.openFileOutput(filename, Context.MODE_PRIVATE);
            fos.write(fileContents.getBytes());
            fos.close();

            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}

Feedback neutru:
“Mai incearca!”
“Poti mai bine!”
“Hai sa mai incercam!”

Feedback pozitiv:
“Da, este corect!”
“Corect!”
“Foarte bine!”
“Super!”
“Excelent!”

Cate o imagine sugestiva pentru fiecare situatie in parte (situatiile sunt scrise cu albastru)

1) Buna ziua!/ Salut!/ Bună dimineața!/ Bună seara! 

	Atunci când Denis se întâlneste cu un prieten spune: 
	Salut! / Te rog/ Cu placere

	Denis s-a intalnit cu un coleg la piscina. Ce ar trebui sa-i spuna Denis?
	Multumesc!, La revedere!, Salut!

	Denis tocmai s-a trezit, iese din camera lui si pe hol se intalneste cu tatal lui. Ce ar trebui sa spuna Denis?
	Buna seara,tata!, Noapte buna, tata!, Buna dimineata, tata!

	In fiecare seara Denis pleaca la ore de dans. Ce ar trebui Denis sa-i zica instuctorului de dans atunci cand ajunge la el? 
	Multumesc!, Cu placere!, Buna seara!

	Dimineața când Denis ajunge la grădiniță/școală îi spune doamnei învățătoare: 
	Bună seara!/Scuzați-mă!/Bună dimineața!

	Atunci când Denis se întâlneste seara cu mama unui prieten îi spune: 
	Te rog!/Bună ziua!/Bună seara!


2) Multumesc

	Atunci când Dens primeste un cadou, oricât de mic, este politicos să spuna: 
	Cu plăcere/Imi pare rău/Mulțumesc

	După ce Denis a mâncat, înainte să se ridice de la masă, ar fi bine să spuna: 
	Nu mi-a plăcut mâncarea/Mulțumesc pentru masă/La revedere

	Când un coleg il servește pe Denis cu pufuleți, iar lui nu îmi plac pufuleții, este politicos să îi spuna: 
	Nu îmi plac pufuleții/Mulțumesc, nu doresc acum/Ai și ceva dulce?

	Cand mama ma ajuta la teme, ar fi bine sa spun: 
	Multumesc!, Cu placere!, Imi pare rau!

	Cand Denis merge la magazin sa-si cumpere ceva si vanzatoarea ii da ceea ce a cerut, ar fi bine sa spuna: 
	Cu plăcere/Imi pare rău/Mulțumesc

3) Te rog

	Denis isi doreste să se joace cu jucăria altui prieten. El poate să îi spuna: 
	Te rog dă-mi jucăria/Dă-mi jucăria/Eu mă joc acum.

	Denis nu isi gaseste ascuțitoarea, iar vârful creionului s-a rupt, Denis poate sa ii ceara colegei de bancă astfel: 
	Dă-mi, te rog, ascuțitoarea ta/Unde este ascuțitoarea ta?Vreau ascuțitoarea

	Denis vrea sa iasa afara. Cum ar trebui sa ii spuna mamei acest lucru?
	Te rog, lasa-ma afara!, Merg afara!, Sa stii ca plec afara!

	Denis vrea sa se incalte dar nu stie sa isi lege sireturile. Ce ii spune mamei?
	Hai ca ma grabesc!, Te rog leaga-mi sireturile!, Vai ce greu te misti!

	Denis merge la magazin si nu gaseste pe raft bomboanele. Ce ii spune vanzatoarei?
	Te rog ajuta-ma sa gasesc bomboanele!, Da-mi mai repede bomboanele!, Unde ai pus bomboanel?!

4) Cu placere

	Denis tocmai i-a oferit o bucată de ciocolată colegului sau, iar acesta i-a mulțumit. Este politicos să îi spuna: 
	Te rog!/Cu plăcere!/Să-mi dai și tu data viitoare.

	Denis l-a ajutat pe prietenul sau să deschidă cutia cu plastelină, iar acesta i-a mulțumit. Denis ar trebui să îi spuna: 
	Data viitoare să te descurci singur/Cu plăcere, și altă dată!/Îmi pare rău!

	Denis a facut un cadou colegei lui de banca. Aceasta i-a multumit. Denis ar trebui sa-i spuna: 
	Super!, Cu placere!, Eu nu am vrut sa-ti dau cadoul!

	Denis a ajutat-o pe bunica sa duca sacosele cu alimente de la piata. Bunica ii multumeste. Denis ar trebui sa spuna: 
	A fost ultima data!, Nu mai fac asta niciodata!, Cu placere!

	Denis merge pe strada si vede o batranica care nu reuseste sa treaca strada. El o ajuta si batranica ii multumeste. Denis ar trebui sa-i spuna: 
	Scuza-ma!, Cu placere!, Salut!

5)Scuza-ma

	În timpul pauzei de joacă Denis trece pe lângă un coleg și, din greșeala, îl împinge. Este politicos să îi spuna: 
	Scuză-mă!/Altcineva m-a împins!/Cu plăcere!

	Denis vorbeste cu niste prieteni si face o afirmatie despre unul dintre ei, iar prietenul lui Denis se supara. Denis ar trebui sa ii spuna: 
	Cu placere! Scuza-ma! Multumesc!

	La ora de desen Denis este neatent si rastoarna un pahar cu apa peste desenul colegei de banca. Denis ar trebui sa spuna: 
	Cu placere!, Scuza-ma!, Multumesc!

	Denis si colegii lui se joaca cu mingea. Din greseala Denis il loveste pe unul dintre colegi cu mingea in fata. Este politicos ca Denis sa spuna: 
	Ce tare!, Multumesc!, Scuza-ma!

	Denis i-a promis prietenului sau ca ajunge la el acasa la ora 15. Denis a intarziat 10 minute. Este politicos ca Denis sa spuna: 
	Am ajuns mai tarziu! Scuza-ma pentru ca am intarziat!, Nu este nicio problema ca am intarziat! 

6) Imi pare rau!

	Din greșeala Denis loveste cu cotul vaza mamei primită de la bunica. Mama apare, iar Denis ar fi bine să îi spuna: 
	Era veche!/Salut!/Îmi pare rău! 

	Denis a uitat sa-si faca tema. La scoala doamna invatatoare ii atrage atentia asupra acestui fapt. Denis ar trebui sa spuna: 
	Cu placere! Imi pare bine ca nu mi-am facut tema!, Imi pare rau ca am uitat sa-mi fac tema!

	Denis a fost invitat la ziua de nastere a Cristinei, dar a uitat sa mearga. Denis ar trebui sa-i spuna: 
	Imi pare rau ca nu am venit! Imi pare rau, dar nu am stiut!, Imi pare rau pentru ca am intarziat!

	Afara ploua si este noroi. Denis merge cu bicicleta si stropeste un trecator. Denis ar trebui sa spuna: 
	Imi pare rau ca v-am stropit!, Multumesc!, Mi-a facut placere sa va stropesc!

	Denis se joaca cu un balon in sala de clasa. Din greseala Denis sparge balonul si isi sperie colegii. Denis ar trebui sa le spuna: 
	Bravo!, Super tare!, Imi pare rau ca v-am speriat!

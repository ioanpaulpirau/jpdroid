package com.apps.jpp.chestionarsituational;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.jpp.jputilslib.FileUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ConfigurationEditor extends AppCompatActivity {
    private FileUtils fileUtils = new FileUtils(this);
    private TextView textViewConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_editor);
        textViewConfiguration = (TextView) findViewById(R.id.editConfigurationId);


        //first try to get the file from the internal storage
        String bufferString = fileUtils.loadTextFileFromInternalStorage(getString(R.string.configfile));
        //if we don't find it there it means that this is the first time we started up the app (or something went horribly wrong)
        if (bufferString.isEmpty())
            resetSettingsToDefaults();
        else
            //load the contents into the edit box
            textViewConfiguration.setText(bufferString);
    }

    /*******************************************************
     * UTILITY FUNCTIONS
     ******************************************************/

    void resetSettingsToDefaults()
    {
        String settingsContents = fileUtils.loadTextFileFromAsset(getString(R.string.configfile));
        fileUtils.saveTextFileToInternalStorage(getString(R.string.configfile), settingsContents);
        textViewConfiguration.setText(settingsContents);

        Toast toast = Toast.makeText(this, R.string.settings_restored, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();

        //now we reload the config
        DataModel.get().reloadConfig();
    }

    boolean saveSettings() {
        String settingsContents = textViewConfiguration.getText().toString();
        Toast toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 0);


        if (!validateChanges()) {
            toast.setText(R.string.invalid_document_changes);
            toast.show();
            return false;
        }

        //do the actual saving
        if (fileUtils.saveTextFileToInternalStorage(getString(R.string.configfile), settingsContents)) {
            toast.setText(R.string.saved_successfully);
            toast.show();

            //now we reload the config
            DataModel.get().reloadConfig();

            return true;
        }

        return false;
    }

    boolean validateChanges() {
        try {
            JSONObject obj = new JSONObject(textViewConfiguration.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /*******************************************************
     * MENU FUNCTIONS
     ******************************************************/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_configuration_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_save: {
                saveSettings();
                return true;
            }
            case R.id.action_reset: {
                resetSettingsToDefaults();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}

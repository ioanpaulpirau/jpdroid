package com.apps.jpp.chestionarsituational;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        Intent intent = getIntent();

        int numQuestions = intent.getIntExtra(getString(R.string.number_of_questions), 10);
        int numTries = intent.getIntExtra(getString(R.string.number_of_tries),10);
        int correctResponses = intent.getIntExtra(getString(R.string.total_correct_responses),10);

        String scoreMessageText = "Ai raspuns la " + numQuestions + " întrebări din " + numTries + " încercări";
        if (correctResponses < numQuestions) scoreMessageText += " cu " + correctResponses + " răspunsuri corecte";
        ((TextView)findViewById(R.id.tv_score)).setText(scoreMessageText);


        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExitApp();
            }
        });

        findViewById(R.id.btn_restart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestartQuiz();
            }
        });
    }

    /***
     * Calls Main Activity and restarts quiz making a reset on the quiz statistics
     */
    private void RestartQuiz() {
        Intent scoreIntent = new Intent(this, MainActivity.class);
        startActivity(scoreIntent);
    }

    /***
     * Takes you back to the home screen, in therms of Android OS it basically closes the app
     */
    public void ExitApp() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //we want to exit app if back was pressed
        ExitApp();
    }

    /*******************************************************
     * MENU FUNCTIONS
     ******************************************************/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_score, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            //open settings activity
            Intent mainIntent = new Intent(this, ConfigurationEditor.class);
            startActivityForResult(mainIntent,0);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

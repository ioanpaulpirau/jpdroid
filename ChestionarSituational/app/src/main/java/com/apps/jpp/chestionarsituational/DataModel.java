package com.apps.jpp.chestionarsituational;

import android.content.Context;
import android.util.Log;

import com.apps.jpp.jputilslib.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class loads, parses and decodes the configuration file. In this configuration file we have all the settings, questions and feedback's that are used.
 * Class is implemented as a lazy load singleton and relies on the Main Activity to be the first caller for this class
 * Created by JP on 17-Jan-16.
 */
public class DataModel {
    private JSONObject configurationParser;
    public QuestionsRecordList questions;
    public FeedbackRecordList feedback;
    public Settings settings;
    private Context appContext;
    private FileUtils fileUtils;

    static String loggerTAG = "ChestionarSituational:DataModel";

    private static DataModel instance;

    public static DataModel get() {
        if(instance == null) instance = getDataModelSync();
        return instance;
    }

    private static synchronized DataModel getDataModelSync() {
        if(instance == null) instance = new DataModel();
        return instance;
    }

    private DataModel(){
        // here you can directly access the Application context calling
        appContext = MainActivity.get().getApplicationContext();

        fileUtils = new FileUtils(appContext);
        configurationParser = new JSONObject();
        parseJSON();
    }

    /*******************************************************
     * HELPER FUNCTIONS
     ******************************************************/

    private void parseJSON() {

        //first try to get the file from the internal storage
        String bufferString = fileUtils.loadTextFileFromInternalStorage(appContext.getString(R.string.configfile));
        //if we don't find it there just load the default config
        if (bufferString.isEmpty())
            bufferString = fileUtils.loadTextFileFromAsset(appContext.getString(R.string.configfile));

        try {
            JSONObject obj = new JSONObject(bufferString);
            settings = new Settings(obj);
            questions = new QuestionsRecordList(obj);
            feedback = new FeedbackRecordList(obj);

            checkConsistency();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkConsistency() {
        if (questions.getCount() < settings.defaultStartIndex) {
            Log.w(loggerTAG, "default start question index higher than questions count! resetting start question index to zero!!");
            settings.defaultStartIndex = 0;
        }
        if (questions.getCount() < settings.numberOfQuestions && settings.preventSameQuestionTwice){
            Log.w(loggerTAG, "repeat question is deactivated and requested number of questions (per questionnaire) is greater that total number of questions. Enabling repeat of same question!");
            settings.preventSameQuestionTwice = false;
        }
    }

    public void reloadConfig() {
        feedback.clear();
        questions.clear();
        parseJSON();
    }

    /*******************************************************
     * INNER CLASSES
     ******************************************************/

    class QuestionRecord {
        int questionIndex;
        String questionText;
        String firstAnswer;
        String secondAnswer;
        String thirdAnswer;
        String imageId;
        int correctAnswer;

        public QuestionRecord(JSONObject obj) throws JSONException {
            questionText = obj.getString("question");
            imageId = obj.getString("imageName");
            firstAnswer = obj.getJSONObject("answers").getString("1");
            secondAnswer = obj.getJSONObject("answers").getString("2");
            thirdAnswer = obj.getJSONObject("answers").getString("3");
            correctAnswer = obj.getInt("correctAnswer");
        }
    }

    class QuestionsRecordList {
        private List<QuestionRecord> questionRecords = new ArrayList<>();

        public QuestionsRecordList(JSONObject obj) {
            try {
                JSONArray array = obj.getJSONArray("Questions");
                for (int i = 0; i < array.length(); i++) {
                    QuestionRecord qr = new QuestionRecord(array.getJSONObject(i));
                    qr.questionIndex = i;
                    questionRecords.add(qr);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public QuestionRecord getQuestionRecord(boolean random, int questionIndex) {
            if (random) {
                Random randomNum = new Random();
                return questionRecords.get(randomNum.nextInt(questionRecords.size()));
            }
            if (questionIndex >= questionRecords.size() - 1)
                return questionRecords.get(0);
            return questionRecords.get(questionIndex);
        }

        public int getCount() {
            return questionRecords.size();
        }

        public void clear(){
            questionRecords.clear();
        }
    }

    class Settings {
        int numberOfQuestions;
        boolean randomSeekStart;
        boolean randomQuestionsPick;
        int defaultStartIndex;
        boolean preventSameQuestionTwice;
        boolean repeatQuestionUntilAnsweredCorrectly;
        boolean displayQuestionNumber;
        boolean giveMessageFeedback;
        boolean giveAudioFeedback;

        public Settings(JSONObject obj) {
            try {
                JSONObject settings = obj.getJSONObject("General");

                numberOfQuestions = settings.getInt("questions_per_questionnaire");
                randomSeekStart = settings.getBoolean("random_questions_start");
                randomQuestionsPick = settings.getBoolean("random_questions_selection");
                defaultStartIndex = settings.getInt("default_start_index");
                preventSameQuestionTwice = settings.getBoolean("prevent_same_question_twice");
                repeatQuestionUntilAnsweredCorrectly = settings.getBoolean("repeat_question_until_answered_correctly");
                displayQuestionNumber = settings.getBoolean("display_question_number");
                giveMessageFeedback = settings.getBoolean("give_message_feedback");
                giveAudioFeedback = settings.getBoolean("give_audio_feedback");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class FeedbackRecordList {
        List<String> positiveFeedback = new ArrayList<>();
        List<String> neutralFeedback = new ArrayList<>();

        public FeedbackRecordList(JSONObject obj) {
            try {
                JSONArray array = obj.getJSONArray("Feedback");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jo_inside = array.getJSONObject(i);
                    if (jo_inside.getString("type").contentEquals("pozitiv"))
                        positiveFeedback.add(jo_inside.getString("value"));
                    else
                        neutralFeedback.add(jo_inside.getString("value"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public String getRandomFeedback(boolean positive) {
            Random randomNum = new Random();
            if (positive && positiveFeedback.size() > 0)
                return positiveFeedback.get(randomNum.nextInt(positiveFeedback.size()));
            if (neutralFeedback.size() > 0)
                return neutralFeedback.get(randomNum.nextInt(neutralFeedback.size()));

            Log.e("JPP","Can't return feedback! lists are empty!!!");
            return "";
        }

        public void clear() {
            positiveFeedback.clear();
            neutralFeedback.clear();
        }
    }
}

package com.apps.jpp.chestionarsituational;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.jpp.jputilslib.MediaUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This is the main activity that displays the questionnaire one question at a time based on the config file
 * Created by JP on 15-Jan-16.
 */
public class MainActivity extends AppCompatActivity {

    DataModel appModel;
    DataModel.QuestionRecord currentQuestion = null;
    MediaUtils mediaUtils = new MediaUtils(this);
    Toast feedbackMessageToast;
    Context context;
    private int questionNumber = 0;
    private int numberOfTries = 0;
    private int correctResponses = 0;
    List<Integer> questionsAsked = new ArrayList<>();
    static String loggerTAG = "ChestionarSituational:MainActivity";


    private static AppCompatActivity instance;
    public static AppCompatActivity get() { return instance; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_main);
        context = this.getBaseContext();
        appModel = DataModel.get();
        feedbackMessageToast = new Toast(context);
        populateQuestion();

        findViewById(R.id.BtnAnswer1Id).setSoundEffectsEnabled(false);
        findViewById(R.id.BtnAnswer2Id).setSoundEffectsEnabled(false);
        findViewById(R.id.BtnAnswer3Id).setSoundEffectsEnabled(false);

        findViewById(R.id.BtnAnswer1Id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcessAnswer(1);
            }
        });

        findViewById(R.id.BtnAnswer2Id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcessAnswer(2);
            }
        });

        findViewById(R.id.BtnAnswer3Id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcessAnswer(3);
            }
        });
    }

    /*******************************************************
     * HELPER FUNCTIONS
     ******************************************************/

    /***
     * Process a given answer, provide feedback to the user and jump to the next question
     * Function knows "" the correct answer from the currentQuestion that is stored in the class
     *
     * @param answerValue the given answer variant that has to be processed
     */
    private void ProcessAnswer(int answerValue) {
        Boolean answerIsCorrect = currentQuestion.correctAnswer == answerValue;
        numberOfTries += 1;

        if (answerIsCorrect) correctResponses += 1;
        //process answer
        if (answerIsCorrect || !appModel.settings.repeatQuestionUntilAnsweredCorrectly) {
            //if all questions were answered then we proceed to closing the questionnaire
            if (questionNumber >= appModel.settings.numberOfQuestions) {
                Intent scoreIntent = new Intent(this, ScoreActivity.class);
                scoreIntent.putExtra(getString(R.string.number_of_questions), appModel.settings.numberOfQuestions);
                scoreIntent.putExtra(getString(R.string.number_of_tries), numberOfTries);
                scoreIntent.putExtra(getString(R.string.total_correct_responses), correctResponses);
                startActivity(scoreIntent);
            } else
                populateQuestion();
        }

        //display toast
        if (appModel.settings.giveMessageFeedback) {
            feedbackMessageToast.cancel();
            feedbackMessageToast = Toast.makeText(context, appModel.feedback.getRandomFeedback(answerIsCorrect), Toast.LENGTH_SHORT);
            feedbackMessageToast.show();
        }
        //play sound
        if (appModel.settings.giveAudioFeedback) {
            String fileName;
            if (answerIsCorrect) fileName = "Ok.mp3";
            else fileName = "NotOk.mp3";
            mediaUtils.playSoundSync(fileName, true);
        }
    }

    /***
     * Request a new question from the model and populate application fields with the contents of the question record
     * Also sets the currentQuestion class variable
     */
    private void populateQuestion() {
        if (currentQuestion == null) {
            int startIndex = appModel.settings.defaultStartIndex;
            if (appModel.settings.randomSeekStart)
                startIndex = new Random().nextInt(appModel.questions.getCount());
            currentQuestion = appModel.questions.getQuestionRecord(appModel.settings.randomQuestionsPick, startIndex);
        } else{
            currentQuestion = appModel.questions.getQuestionRecord(appModel.settings.randomQuestionsPick, currentQuestion.questionIndex + 1);
            while (appModel.settings.preventSameQuestionTwice && questionsAsked.contains(currentQuestion.questionIndex)){
                currentQuestion = appModel.questions.getQuestionRecord(appModel.settings.randomQuestionsPick, currentQuestion.questionIndex + 1);
                Log.i(loggerTAG, "Randomize returned same question twice, seeking to another question");
            }
        }

        questionNumber += 1;
        questionsAsked.add(currentQuestion.questionIndex);

        String questionString = "";
        if (appModel.settings.displayQuestionNumber)
            questionString += (questionNumber) + ". ";
        questionString += currentQuestion.questionText;


        ((TextView) findViewById(R.id.txtQuestionId)).setText(questionString);
        ((TextView) findViewById(R.id.BtnAnswer1Id)).setText(currentQuestion.firstAnswer);
        ((TextView) findViewById(R.id.BtnAnswer2Id)).setText(currentQuestion.secondAnswer);
        ((TextView) findViewById(R.id.BtnAnswer3Id)).setText(currentQuestion.thirdAnswer);
    }

    /****
     * Resets all question statistics (numbering, answered questions, number of tries, etc.)
     */
    void restartQuiz() {
        questionNumber = 0;
        numberOfTries = 0;
        correctResponses = 0;
        populateQuestion();
    }

    //TODO: handle change on the settings file by reloading DataModel!

    /*******************************************************
     * MENU FUNCTIONS
     ******************************************************/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //open settings activity
            Intent mainIntent = new Intent(this, ConfigurationEditor.class);
            startActivity(mainIntent);

            return true;
        }

        if (id == R.id.action_new_quiz) {
            // reset scoring & question numbering, randomize questions pool
            restartQuiz();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
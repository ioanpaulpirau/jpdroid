{
	"comment": "Folosește site-ul http://www.jsoneditoronline.org/ pentru a opera modificări pe acest fișier și respectă structurile așa cum sunt ele definite în conținutul actual. În caz contrar, s-ar putea ca aplicația să nu mai funcționeze corespunzător!",
	"General": {
		"questions_per_questionnaire": "10",
		"random_questions_start": "true",
		"random_questions_selection": "true",
		"default_start_index": "0",
		"prevent_same_question_twice": "true",
		"repeat_question_until_answered_correctly": "true",
		"display_question_number": "true",
		"give_message_feedback": "true",
		"give_audio_feedback": "true"
	},
	"Feedback": [{
		"type": "neutru",
		"value": "Mai incearca!"
	},
	{
		"type": "neutru",
		"value": "Poti mai bine!"
	},
	{
		"type": "neutru",
		"value": "Hai sa mai incercam!"
	},
	{
		"type": "pozitiv",
		"value": "Da, este corect!"
	},
	{
		"type": "pozitiv",
		"value": "Corect!"
	},
	{
		"type": "pozitiv",
		"value": "Foarte bine!"
	},
	{
		"type": "pozitiv",
		"value": "Super!"
	},
	{
		"type": "pozitiv",
		"value": "Excelent!"
	}],
	"Questions": [{
		"question": "Atunci când Denis se întâlneste cu un prieten spune : ",
		"imageName": "",
		"answers": {
			"1": "Salut!",
			"2": "Te rog",
			"3": "Cu placere"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Denis s-a intalnit cu un coleg la piscina. Ce ar trebui sa-i spuna Denis ?",
		"imageName": "",
		"answers": {
			"1": "Multumesc!",
			"2": "La revedere!",
			"3": "Salut"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Denis tocmai s-a trezit, iese din camera lui si pe hol se intalneste cu tatal lui. Ce ar trebui sa spuna Denis ?",
		"imageName": "",
		"answers": {
			"1": "Buna seara,tata!",
			"2": "Noapte buna, tata!",
			"3": "Buna dimineata, tata!"
		},
		"correctAnswer": "3"
	},
	{
		"question": "In fiecare seara Denis pleaca la ore de dans. Ce ar trebui Denis sa-i zica instuctorului de dans atunci cand ajunge la el ?",
		"imageName": "",
		"answers": {
			"1": "Multumesc!",
			"2": "Cu placere!",
			"3": "Buna seara!"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Dimineața când Denis ajunge la grădiniță/școală îi spune doamnei învățătoare :",
		"imageName": "",
		"answers": {
			"1": "Bună seara!",
			"2": "Scuzați-mă!",
			"3": "Bună dimineața!"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Atunci când Denis se întâlneste seara cu mama unui prieten îi spune : ",
		"imageName": "",
		"answers": {
			"1": "Te rog!",
			"2": "Buna ziua!",
			"3": "Buna seara!"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Atunci când Denis primeste un cadou, oricât de mic, este politicos să spuna: ",
		"imageName": "",
		"answers": {
			"1": "Cu plăcere",
			"2": "Imi pare rău",
			"3": "Mulțumesc"
		},
		"correctAnswer": "3"
	},
	{
		"question": "După ce Denis a mâncat, înainte să se ridice de la masă, ar fi bine să spuna: ",
		"imageName": "",
		"answers": {
			"1": "Nu mi-a plăcut mâncarea",
			"2": "Mulțumesc pentru masă",
			"3": "La revedere"
		},
		"correctAnswer": "2"
	},
	{
		"question": "Când un coleg îl servește pe Denis cu pufuleți, iar lui nu îi plac pufuleții, este politicos să îi spuna: ",
		"imageName": "",
		"answers": {
			"1": "Nu îmi plac pufuleții",
			"2": "Mulțumesc, nu doresc acum",
			"3": "Ai și ceva dulce?"
		},
		"correctAnswer": "2"
	},
	{
		"question": "Cand mama mă ajută la teme, ar fi bine să spun: ",
		"imageName": "",
		"answers": {
			"1": "Multumesc!",
			"2": "Cu placere!",
			"3": "Imi pare rau!"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Cand Denis merge la magazin sa-si cumpere ceva si vanzatoarea îi dă ceea ce a cerut, ar fi bine sa spuna: ",
		"imageName": "",
		"answers": {
			"1": "Cu plăcere",
			"2": "Imi pare rău",
			"3": "Mulțumesc"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Denis isi doreste să se joace cu jucăria altui prieten. El poate să îi spuna: ",
		"imageName": "",
		"answers": {
			"1": "Te rog dă-mi jucăria",
			"2": "Dă-mi jucăria",
			"3": "Eu mă joc acum"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Denis nu isi gaseste ascuțitoarea, iar vârful creionului s-a rupt, Denis poate sa ii ceara colegei de bancă astfel: ",
		"imageName": "",
		"answers": {
			"1": "Dă-mi, te rog, ascuțitoarea ta",
			"2": "Unde este ascuțitoarea ta?",
			"3": "Vreau ascuțitoarea"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Denis vrea sa iasa afara. Cum ar trebui sa ii spuna mamei acest lucru?",
		"imageName": "",
		"answers": {
			"1": "Te rog, lasa-ma afara!",
			"2": "Merg afara!",
			"3": "Sa stii ca plec afara!"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Denis vrea să se incalte dar nu știe sa își lege sireturile. Ce îi spune mamei? ",
		"imageName": "",
		"answers": {
			"1": "Hai ca ma grabesc!",
			"2": "Te rog leaga-mi sireturile!",
			"3": "Vai ce greu te misti!"
		},
		"correctAnswer": "2"
	},
	{
		"question": "Denis merge la magazin si nu gaseste pe raft bomboanele. Ce ii spune vanzatoarei? ",
		"imageName": "",
		"answers": {
			"1": "Te rog ajuta-ma sa gasesc bomboanele!",
			"2": "Da-mi mai repede bomboanele!",
			"3": "Unde ai pus bomboanele?!"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Denis tocmai i-a oferit o bucată de ciocolată colegului sau, iar acesta i-a mulțumit. Este politicos să îi spuna: ",
		"imageName": "",
		"answers": {
			"1": "Te rog!",
			"2": "Cu plăcere!",
			"3": "Să-mi dai și tu data viitoare"
		},
		"correctAnswer": "2"
	},
	{
		"question": "Denis l-a ajutat pe prietenul sau să deschidă cutia cu plastelină, iar acesta i-a mulțumit. Denis ar trebui să îi spuna: ",
		"imageName": "",
		"answers": {
			"1": "Data viitoare să te descurci singur",
			"2": "Cu plăcere, și altă dată!",
			"3": "Îmi pare rău!"
		},
		"correctAnswer": "2"
	},
	{
		"question": "Denis a facut un cadou colegei lui de banca. Aceasta i-a multumit. Denis ar trebui sa-i spuna: ",
		"imageName": "",
		"answers": {
			"1": "Super!",
			"2": "Cu placere!",
			"3": "Eu nu am vrut sa-ti dau cadoul!"
		},
		"correctAnswer": "2"
	},
	{
		"question": "Denis a ajutat-o pe bunica sa duca sacosele cu alimente de la piata. Bunica ii multumeste. Denis ar trebui sa spuna: ",
		"imageName": "",
		"answers": {
			"1": "A fost ultima data!",
			"2": "Nu mai fac asta niciodata!",
			"3": "Cu placere!"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Denis merge pe stradă și vede o bătrânică care nu reușește să treacă strada. El o ajută și bătrânica îi mulțumește. Denis ar trebui să-i spună: ",
		"imageName": "",
		"answers": {
			"1": "Scuză-mă!",
			"2": "Cu plăcere!",
			"3": "Salut!"
		},
		"correctAnswer": "2"
	},
	{
		"question": "În timpul pauzei de joacă Denis trece pe lângă un coleg și, din greșeala, îl împinge. Este politicos să îi spuna: ",
		"imageName": "",
		"answers": {
			"1": "Scuză-mă!",
			"2": "Altcineva m-a împins!",
			"3": "Cu plăcere!"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Denis vorbeste cu niste prieteni si face o afirmatie despre unul dintre ei, iar prietenul lui Denis se supara. Denis ar trebui sa ii spuna: ",
		"imageName": "",
		"answers": {
			"1": "Cu placere!",
			"2": "Scuza-ma!",
			"3": "Multumesc!"
		},
		"correctAnswer": "2"
	},
	{
		"question": "La ora de desen Denis este neatent si rastoarna un pahar cu apa peste desenul colegei de banca. Denis ar trebui sa spuna: ",
		"imageName": "",
		"answers": {
			"1": "Cu placere!",
			"2": "Scuza-ma!",
			"3": "Multumesc!"
		},
		"correctAnswer": "2"
	},
	{
		"question": "Denis si colegii lui se joaca cu mingea. Din greseala Denis il loveste pe unul dintre colegi cu mingea in fata. Este politicos ca Denis sa spuna: ",
		"imageName": "",
		"answers": {
			"1": "Ce tare!",
			"2": "Multumesc!",
			"3": "Scuza-ma!"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Denis i-a promis prietenului sau ca ajunge la el acasa la ora trei după-amiază. Denis a intarziat 10 minute. Este politicos ca Denis sa spuna: ",
		"imageName": "",
		"answers": {
			"1": "Am ajuns mai tarziu!",
			"2": "Scuza-ma pentru ca am intarziat!",
			"3": "Nu este nicio problema ca am intarziat!"
		},
		"correctAnswer": "2"
	},
	{
		"question": "Din greșeala Denis loveste cu cotul vaza mamei primită de la bunica. Mama apare, iar Denis ar fi bine să îi spuna: ",
		"imageName": "",
		"answers": {
			"1": "Era veche!",
			"2": "Salut!",
			"3": "Îmi pare rău!"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Denis a uitat sa-si faca tema. La scoala doamna invatatoare ii atrage atentia asupra acestui fapt. Denis ar trebui sa spuna: ",
		"imageName": "",
		"answers": {
			"1": "Cu placere!",
			"2": "Imi pare bine ca nu mi-am facut tema!",
			"3": "Imi pare rau ca am uitat sa-mi fac tema!"
		},
		"correctAnswer": "3"
	},
	{
		"question": "Denis a fost invitat la ziua de nastere a Cristinei, dar a uitat sa mearga. Denis ar trebui sa-i spuna: ",
		"imageName": "",
		"answers": {
			"1": "Imi pare rau ca nu am venit!",
			"2": "Imi pare rau, dar nu am stiut!",
			"3": "Imi pare rau pentru ca am intarziat!"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Afara ploua si este noroi. Denis merge cu bicicleta si stropeste un trecator. Denis ar trebui sa spuna: ",
		"imageName": "",
		"answers": {
			"1": "Imi pare rau ca v-am stropit!",
			"2": "Multumesc!",
			"3": "Mi-a facut placere sa va stropesc!"
		},
		"correctAnswer": "1"
	},
	{
		"question": "Denis se joaca cu un balon in sala de clasa. Din greseala Denis sparge balonul si isi sperie colegii. Denis ar trebui sa le spuna: ",
		"imageName": "",
		"answers": {
			"1": "Bravo!",
			"2": "Super tare!",
			"3": "Imi pare rau ca v-am speriat!"
		},
		"correctAnswer": "3"
	}]
}
package com.apps.jpp.anatomie;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.Image;
import android.media.MediaPlayer;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.widget.ImageView;

import java.io.IOException;

/**
 * Created by JP on 12-Jan-16.
 */
public class AppUtils {

    public static String LoggerTopic = "LocalAppLogger";

    private Context m_AppContext;
    private MediaPlayer m_MediaPlayer = new MediaPlayer();

    //constructor
    public AppUtils(Context appContext) {
        m_AppContext = appContext;
    }

    ///////////////////////////// Sounds /////////////////////////////
    //////////////////////////////////////////////////////////////////

    //play sound
    public void playSoundSync(String fileName, boolean overwrite) {
        try {
            AssetFileDescriptor afd;
            afd = m_AppContext.getAssets().openFd(fileName);

            if (!m_MediaPlayer.isPlaying() || overwrite) {
                m_MediaPlayer.reset();
                m_MediaPlayer.setDataSource(afd.getFileDescriptor(),
                        afd.getStartOffset(),
                        afd.getLength());
                m_MediaPlayer.prepareAsync();
                m_MediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    ///////////////////////////// Device density /////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    private float getDensity(){
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity)m_AppContext).getWindowManager().getDefaultDisplay().getMetrics(dm);

        float logicalDensity = dm.density;

        return logicalDensity;
    }

    public int getDP(int paramPixels){
        int px = (int) Math.ceil(paramPixels/getDensity());

        return px;
    }

    public int getPX(int paramPixels){
        int px = (int) Math.ceil(paramPixels*getDensity());

        return px;
    }

    public void getPercent(int paramPixelsX, int paramPixelsY, ImageView iv, float percentX, float percentY)
    {
        percentX = (paramPixelsX*100)/iv.getWidth();
        percentY = (paramPixelsY*100)/iv.getHeight();
    }


    public boolean pointInArea(int pX, int pY, int startX, int endX, int startY, int endY){

        if (pX < startX) return false;
        if (pX > endX) return false;
        if (pY < startY) return false;
        if (pY > endY) return false;

        return true;
    }

    public boolean pointInAreaPx(int pX, int pY, int startX, int endX, int startY, int endY){
        int lpX = getDP(pX);
        int lpY = getDP(pY);

        if (lpX < startX) return false;
        if (lpX > endX) return false;
        if (lpY < startY) return false;
        if (lpY > endY) return false;

        return true;
    }

    public boolean pointInPercent(ImageView iv, int pX, int pY, int startX, int endX, int startY, int endY){

        float percentX = (pX*100)/iv.getWidth();
        float percentY = (pY*100)/iv.getHeight();

        if (percentX < startX) return false;
        if (percentX > endX) return false;
        if (percentY < startY) return false;
        if (percentY > endY) return false;

        return true;
    }

}

package com.apps.jpp.anatomie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    //global vars
    AppUtils m_AppUtils = new AppUtils(this);
    ImageView m_imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupClickHandler();
    }

    private void setupClickHandler() {
        ImageView imgView = (ImageView)findViewById(R.id.imageViewBaiat);
        imgView.setSoundEffectsEnabled(false);

        m_imgView = imgView;

        imgView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent m) {

                String imgX = Float.toString(m_AppUtils.getDP((int) m_imgView.getX()));
                String imgY = Float.toString(m_AppUtils.getDP((int) m_imgView.getY()));
                String imgW = Float.toString(m_AppUtils.getDP((int) m_imgView.getWidth()));
                String imgH = Float.toString(m_AppUtils.getDP((int) m_imgView.getHeight()));
                Log.i(m_AppUtils.LoggerTopic, "X: " + imgX + " - Y: " + imgY + " - W: " + imgW + " - H: " + imgH);

                String logTextX = Float.toString(m.getX());
                String logTextY = Float.toString(m.getY());
                String logTextDpX = Float.toString(m_AppUtils.getDP((int) m.getX()));
                String logTextDpY = Float.toString(m_AppUtils.getDP((int) m.getY()));
                Log.i(m_AppUtils.LoggerTopic, "X: " + logTextX + " - Y: " + logTextY + "DpX: " + logTextDpX + " - DpY: " + logTextDpY);


                //head area
                if (m_AppUtils.pointInPercent((ImageView) v, ((int) m.getX()), ((int) m.getY()), 37, 62, 1, 17))
                    m_AppUtils.playSoundSync("sounds/cap.mp3", true);

                //neck area
                if (m_AppUtils.pointInPercent((ImageView) v, ((int) m.getX()), ((int) m.getY()), 42, 57, 18, 22))
                    m_AppUtils.playSoundSync("sounds/gat.mp3", true);

                //chest area
                if (m_AppUtils.pointInPercent((ImageView) v, ((int) m.getX()), ((int) m.getY()), 35, 65, 23, 49))
                    m_AppUtils.playSoundSync("sounds/piept.mp3", true);

                //belly area
                if (m_AppUtils.pointInPercent((ImageView) v, ((int) m.getX()), ((int) m.getY()), 35, 65, 35, 50))
                    m_AppUtils.playSoundSync("sounds/burta.mp3", false);

                //hands area
                if (m_AppUtils.pointInPercent((ImageView) v, ((int) m.getX()), ((int) m.getY()), 10, 25, 25, 60))
                    m_AppUtils.playSoundSync("sounds/mana.mp3", false);
                if (m_AppUtils.pointInPercent((ImageView) v, ((int) m.getX()), ((int) m.getY()), 70, 90, 25, 60))
                    m_AppUtils.playSoundSync("sounds/mana.mp3", false);

                //feet area
                if (m_AppUtils.pointInPercent((ImageView) v, ((int) m.getX()), ((int) m.getY()), 30, 70, 60, 99))
                    m_AppUtils.playSoundSync("sounds/picior.mp3", false);

/*
                //head area
                if (m_AppUtils.pointInArea(((int) m.getX()), ((int) m.getY()), 215, 470, 50, 265))
                    m_AppUtils.playSoundSync("sounds/cap.mp3", false);

                //feet area
                if (m_AppUtils.pointInArea(((int) m.getX()), ((int) m.getY()), 132, 544, 1050, 1550))
                    m_AppUtils.playSoundSync("sounds/picior.mp3", false);

                //belly area
                if (m_AppUtils.pointInArea(((int) m.getX()), ((int) m.getY()), 250, 400, 550, 700))
                    m_AppUtils.playSoundSync("sounds/burta.mp3", false);
*/
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
